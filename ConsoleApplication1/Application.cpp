// Application.cpp: ���������� ����� ����� ��� ����������� ����������.
//
#pragma once
#include "stdafx.h"
#include <string>
#include <iostream>
#include "..\\�������������\Group.h"
#include "..\\�������������\Table.h"

using namespace std;

const char *Names[] = { "Unknown", "DayBudget", "EveningBudget", "Pay" };
const char *Menu[] = {  "0. Quit", "1. New Element", "2. Create new group", "3. Find Element with index", "4. Chance group's characteristics", "5. Delete Element with index", "6. Delete the group's description", "7. Show all", "8. Show all groups of one department", "9. Get an information about the ballance for a year" },
*Choice = "Make your choice",
*Msg = "You are wrong; repeate please";

int Answer(const char *alt[], int n);
int Add(Table&), Createnew(Table&), Find(Table&), Change(Table&), Del(Table&), Del_descr(Table&), Show(Table&), Show_dep(Table&), GetInfo(Table&);
int(*Funcs[])(Table&) = { NULL, Add, Createnew, Find, Change, Del, Del_descr, Show, Show_dep, GetInfo };
const int Num = sizeof(Menu) / sizeof(Menu[0]);


void main()
{
	Table ar;
	int ind;
	while (ind = Answer(Menu, Num))
		Funcs[ind](ar);
	cout << "That's all. Buy!" << endl;
}

int Answer(const char *alt[], int n)
{
	int answer;
	const char *prompt = Choice;
	cout << "What do you want to do:" << endl;
	for (int i = 0; i < n; i++)
		cout << alt[i] << endl;
	do{
		cout << prompt << ": -> ";
		prompt = Msg;
		cin >> answer;
	} while (answer < 0 || answer >= n);
	cin.ignore(80, '\n');
	return answer;
}

const char *Sh[] = { "1. Create DayBudget", "2. Create EveningBudget", "3. Create Pay", "0. Quit" };
const int NumSh = sizeof(Sh) / sizeof(Sh[0]);

int Add(Table& a)
{
	Group *ptr = nullptr;
	DayBudget D;
	EveningBudget E;
	Pay P;
	string name;
	int i; 
	while (i = Answer(Sh, NumSh)){
		cout << "Enter a group name: --> ";
		cin >> name;
		TableIt it;
		it = a.find(name);
		if (it != a.end()){
			cout << "Element with this key is already here. " << endl;
			continue;
		}
		switch (i){
		case 1:
			ptr = &D;
			break;
		case 2:
			ptr = &E;
			break;
		case 3:
			ptr = &P;
			break;
		}
		cin >> (*ptr);
		//cin.ignore(80, '\n');
		a.insert(name, ptr);
	}
	return 0;
}

const char *Sh2[] = { "1. Change the number of department", "2. Change the period of education", "3. Change the specialization", "4. Change contingent of students", "0. Quit" };
const int NumSh2 = sizeof(Sh2) / sizeof(Sh2[0]);

int Createnew(Table &a){
	Group *ptr = nullptr;
	DayBudget D;
	EveningBudget E;
	Pay P;
	string name;
	int i, j;
	cout << "Enter a name of a new group : --> ";
	cin >> name;
	TableIt it;
	it = a.find(name);
	if (it != a.end()){
		cout << "Group with this name excists. You can change this group." << endl;
		while (i = Answer(Sh2, NumSh2)){
			switch (i){
			case 1:
				a.change_kaf(name);
				break;
			case 2:
				a.change_years(name);
				break;
			case 3:
				a.change_spec(name);
				break;
			case 4:
				a.change_kont(name);
				break;
			}
		}
	}
	else{
		cout << "Enter the type of group:\n 1 - DayBudget\n 2 - EveningBudget\n 3 - Pay" << endl;
		cin >> j;
		switch (j){
		case 1:
			ptr = &D;
			break;
		case 2:
			ptr = &E;
			break;
		case 3:
			ptr = &P;
			break;
		}
		cin >> (*ptr);
		a.insert(name, ptr);
	}
	return 0;
}

int Find(Table &a)
{
	string name;
	TableIt it;
	const Group *ptr = nullptr;
	cout << "Enter a group name: --> ";
	cin >> name;
	it = a.find(name);
	if (it == a.end()){
		cout << "The group with Name \"" << name << "\" is not in container"
			<< endl;
		return -1;
	}
	ptr = (*it).second;
	cout << "The group with Name \"" << (*it).first << "\" is a " << Names[ptr->Get_Form()]
		<< endl;
	cout << (*ptr) << endl;
	return 0;
}

const char *Sh1[] = { "1. Change the headcount", "2. Change the scholarship", "3. Change the number of scholars", "4. Change the payment", "0. Quit" };
const int NumSh1 = sizeof(Sh1) / sizeof(Sh1[0]);

int Change(Table &a){
	string code;
	cout << "Enter the name of group you want to change -->" << endl;
	cin >> code;
	TableIt it;
	int ans;
	it = a.find(code);
	if (it == a.end())
		cout << "The group with number " << code << " is not in container" << endl;
	else
	{
		while (ans = Answer(Sh1, NumSh1)){
			switch (ans)
			{
			case 1:
				a.change_n(code);
				break;
			case 2:
				a.change_scholarship(code);
				break;
			case 3:
				a.change_scholars(code);
				break;
			case 4:
				a.change_payment(code);
				break;
			}
		}
	}
	return 0;
}

int Del(Table &a)
{
	string name;
	TableIt it;
	cout << "Enter a group name: --> ";
	cin >> name;
	it = a.find(name);
	while (it == a.end()){
		cout << "Element with this key is not in container. Try again, please" << endl;
		cin >> name;
		it = a.find(name);
	}
	a.deleteelem(name);
	return 0;
}

int Del_descr(Table &a){
	string name;
	TableIt it;
	cout << "Enter a group name: --> ";
	cin >> name;
	it = a.find(name);
	while (it == a.end()){
		cout << "Element with this key is not in container. Try again, please" << endl;
		cin >> name;
		it = a.find(name);
	}
	a.deletedescriber(name);
	return 0;
}

int Show(Table& a)
{
	cout << "All groups:" << endl;
	a.show();
	return 0;
}

int Show_dep(Table& a){
	a.show_kaf();
	return 0;
}

int GetInfo(Table& a){
	int k;
	k = a.get_report();
	cout << "The ballance for a year: " << k << endl;
	return 0;
}
