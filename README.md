Baranova E.A. Group K03-122. Variant 7 "learning groups"
                         

  What is this program about?
  -----------

  This program allows user to control accounting of training groups in the Institute. 
  The user can work with three types of study groups: 
  daily budget group, evening budget group and pay group. 
  In the course of working the program creates a table that contains a data about all groups.
  The table row is a group name and a pointer to the element that holds all data about the group with this name. 
  The user can modify the characteristics of groups, to change the total number of groups,
  to obtain information about all the groups at once, or each separately, 
  to receive the report on the balance cash for the academic year.

  The Latest Version
  ------------------

  All project classes is realised using the standard library STL

  Documentation
  -------------

  The documentation available in 
  file:///C:/Users/user/Documents/Visual%20Studio%202013/Projects/�������������/�������������/html/annotated.html

  Installation
  ------------

  Please see the file called ������� ������. File type is the Microsoft Visual Studio Solution (.sln)

