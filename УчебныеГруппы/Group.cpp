#include "stdafx.h"
#include "Group.h"
#include <iostream>
#include <string>
using namespace std;

ostream& operator <<(ostream &os, const Group &p)
{
	return p.show(os);
}

istream& operator >>(istream &is, Group &p)
{
	return p.get(is);
}

ostream& Group::show(ostream& os) const
{
	return os << "\n specialty:" << spec << "\n headcount:" << n << "\n department:" << kaf << "\n study period:" << years ;
}

ostream& DayBudget::show(ostream& os)const
{
	Group::show(os);
	return os << "\n scholarship:" << stip << "\n scholars:" << nstip << endl;
}

ostream& EveningBudget::show(ostream& os)const
{
	Group::show(os);
	return os << "\n contingent:" << kont << "\n qwalify:" << qwalify << endl;
}

ostream& Pay::show(ostream& os)const
{
	Group::show(os);
	return os << "\n contract number:" << contractnum << "\n payment:" << payment << endl;
}

istream& Group::get(istream& is)
{
	cout<<"Put the number of students:"<<endl;
	is >> n;
	cout << "Put the specialization:" << endl;
	is >> spec;
	cout << "Put the department:" << endl;
	is >> kaf;
	cout << "Put the study period:" << endl;
	return is >> years;
}

istream& DayBudget::get(istream& is)
{
	Group::get(is);
	cout << "Put how many scholars:" << endl;
	is >> nstip;
	while (nstip > n){
		cout << "You're wrong. Repeat, please";
		is >> nstip;
	}
	cout << "Put the scholarship:" << endl;
	return is >> stip;
}

istream& EveningBudget::get(istream& is)
{
	Group::get(is);
	cout << "Put the contingent:" << endl;
	is >> kont;
	cout << "Put the qwalify:" << endl;
	return is >> qwalify;
}

istream& Pay::get(istream& is)
{
	Group::get(is);
	cout << "Put the contract number:" << endl;
	is >> contractnum;
	cout << "Put the payment:" << endl;
	return is >> payment;
}

void Group::Set_n()
{
	cout << "enter new number of students -->";
	int n1;
	cin >> n1;
	n = n1;
}

int Group::Get_n() 
{ 
	return n; 
}

void Group::Set_kaf(){
	cout << "enter new department -->";
	int kaf1;
	cin >> kaf1;
	kaf = kaf1;
}

int Group::Get_kaf() 
{ 
	return kaf; 
}

void Group::Set_years(){
	cout << "enter new period of education -->";
	int years1;
	cin >> years1;
	years = years1;
}

void Group::Set_spec(){
	cout << "enter new specialization -->";
	string spec1;
	cin >> spec1;
	spec = spec1;
}

string Group::Get_spec() 
{ 
	return spec; 
}

int DayBudget::Get_stip()const
{
	return stip;
}

void DayBudget::Set_stip()
{
	cout << "enter new scholarship -->";
	int st;
	cin >> st;
	stip = st;
}

int DayBudget::Get_nstip()const
{
	return nstip;
}

void DayBudget::Set_nstip()
{
	cout << "enter new number of scholars";
	int nst;
	cin >> nst;
	while (nst > n){
		cout << "You're wrong. Repeat, please";
		cin >> nst;
	}
	nstip = nst;
}

void EveningBudget::Set_kont(){
	cout << "enter new kontingent -->";
	int kont1;
	cin >> kont1;
	kont = kont1;
}

int Pay::Get_payment()const
{
	return payment;
};

void Pay::Set_payment()
{
	cout << "enter new payment -->";
	int cost;
	cin >> cost;
	payment = cost;
}