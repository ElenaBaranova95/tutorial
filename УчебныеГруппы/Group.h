#pragma once
#ifndef _GROUP_H_
#define _GROUP_H_
#include <iostream>
#include <string>

using namespace std;

class Group
{
protected:
	int n;                               //!�����������
	short kaf;                           //!����� ������������� �������
	short years;                         //!���� ��������
	string spec;                         //!�������������
	virtual ostream& show(ostream&)const = 0;
	virtual istream& get(istream&);// = 0;
public:
	Group(){};
	virtual Group* clone()const = 0;
	friend ostream &operator <<(ostream &, const Group &);   //!�������� ���������� � ������
	friend istream &operator >>(istream &, Group &);    
	void Set_n();                                            //!�������� ����������� ������
	int Get_n();                                             //!������� ����������� ������
	int Get_kaf();											 //!������� ����� ������������� �������
	void Set_kaf();
	void Set_years();
	string Get_spec();										 //!�������� �������������
	void Set_spec();
	virtual int Get_Form()const = 0;                         //!������� ����� ��������
	virtual ~Group(){};
};

class DayBudget :public Group{
protected:
	int stip;                                 //!������ ���������
	int nstip;                                //!���������� ���������, ���������� ���������
	virtual ostream& show(ostream&)const;
	virtual istream& get(istream&);
public:
	DayBudget(){};
	virtual DayBudget* clone()const { return new DayBudget(*this); };
	virtual int Get_Form()const { return 1; }
	int Get_stip()const;                   //!�������� ������ ���������
	void Set_stip();                       //!�������� ������ ���������
	int Get_nstip()const;                  //!�������� ���-�� ����� �� ����������
	void Set_nstip();                      //!�������� ���-�� ����� �� ����������
};

class EveningBudget :public Group{
protected:
	int kont;                      //!���������� ��������
	string qwalify;                   //!������������ ����������
	virtual ostream& show(ostream&)const;
	virtual istream& get(istream&);
public:
	EveningBudget(){};
	virtual EveningBudget* clone()const { return new EveningBudget(*this); }
	virtual int Get_Form()const { return 2; }
	void Set_kont();
};

class Pay :public Group
{
protected:
	int contractnum;                      //!����� ���������
	int payment;                          //!������ ����� �� �������
	virtual ostream& show(ostream&)const;
	virtual istream& get(istream&);
public:
	Pay(){};
	virtual Pay* clone()const { return new Pay(*this); }
	virtual int Get_Form()const { return 3; }
	int Get_payment()const;                //!�������� ������ ����������� �����
	void Set_payment();                    //!�������� ������ ����������� �����
};
#endif