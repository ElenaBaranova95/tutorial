#ifndef _TABLE_H_
#define _TABLE_H_
#include <iostream>
#include <map>
#include <string>
#include "Group.h"
using namespace std;

ostream & operator <<(ostream &, const pair<string, Group *> &); 

class Table{
private:
	map<const string, Group *> arr;
public:

	Table(const Table &);
	Table() {};
	~Table();

	Table& operator = (const Table &);
	void insert(const string &, const Group *);              //!�������� ����� �������
	const Group * operator [](const string &) const;
	void deleteelem(const string &);                         //!������� ������� �� �������
	void deletedescriber(const string &);                    //!��������� �������� ������
	void show();                                                //!�������� ���������� �������
	void show_kaf();										//!�������� ������ ��������� �������
	
	void change_kont(const string &);
	void change_spec(const string &);
	void change_years(const string &);
	void change_kaf(const string &);
	void change_n(const string &);
	void change_scholarship(const string &);
	void change_scholars(const string &);
	void change_payment(const string &);

	int get_report();                       //!����� � �������� �� ���

	friend class TableIt;
	typedef TableIt Iterator;
	Iterator begin();
	Iterator end();
	Iterator find(const string &);                           //!����� ������� �� �������
};

class TableIt{
private:
	map<const string, Group *>::iterator cur;
public:
	TableIt(){}
	TableIt(map<const string, Group *>::iterator it) :cur(it){}
	int operator !=(const TableIt &) const;
	int operator ==(const TableIt &) const;
	pair<const string, Group *> & operator *();
	TableIt & operator ++();
	TableIt operator ++(int);
};
#endif


/*#pragma once
#include "Group.h"
#include <iostream>
using namespace std;

struct Elem
{
	string index;
	Group *gruppa;
	Elem() :index(string()), gruppa(NULL){ }
	Elem(const string &name) : index(name), gruppa(NULL){ }
};

class Table
{
private:
	Elem *arr;
	int cnt, cur;
	int getPos(const string &) const;
public:

	Table() : cnt(1), cur(0), arr(new Elem[1]){ }
	Table(const Table&a) :cnt(a.cnt), cur(a.cur), arr(new Elem[a.cnt])
	{
		for (int i = 0; i < cnt; i++){
			arr[i].index = a.arr[i].index;
			arr[i].gruppa = a.arr[i].gruppa->clone();
		}
	}
	~Table(){ delete[] arr; }

	void insert(const string &, const Group *);  //�������� ����� �������
	Group *& operator [ ](const string &);
	Elem finding(const string &);                 //����� ������� �� �������
	Table& operator = (const Table &);
	void deleteelem(const string &);              //������� ������� �� �������
	void show();	                              //�������� ���������� �������
	friend class TableIt;
	typedef TableIt Iterator;
	Iterator begin();
	Iterator end();
	Iterator find(const string &s) const{
		int i = getPos(s);
		if (i < 0) i = cur;
		return TableIt(this->arr + i);
	}
};

class TableIt {
private:
	Elem *cur;
public:
	TableIt() :cur(0){ }
	TableIt(Elem *a) :cur(a){ }
	Elem & operator *();
	TableIt & operator ++();
	TableIt operator ++(int);
	int operator !=(const TableIt &) const;
	int operator ==(const TableIt &) const;
};
*/