#include "stdafx.h"
#include "Table.h"
#include <string>
using namespace std;

ostream & operator <<(ostream & os, const pair<string, Group *> & p){
	os << '"' << p.first << '"';
	if (p.second)
		os << " - " << (*p.second);
	else
		os << "is empty";
	return os;
}

Table::Table(const Table & a){
	map<const string, Group *>::const_iterator p;
	for (p = a.arr.begin(); p != a.arr.end(); ++p)
		arr.insert(make_pair(p->first, p->second->clone()));
}

Table::~Table(){
	map<const string, Group *>::iterator p;
	for (p = arr.begin(); p != arr.end(); ++p){
		delete p->second;
		p->second = nullptr;}
}


void Table::deleteelem(const string & ss){
	map<const string, Group *>::iterator p;
	p = arr.find(ss);
	arr.erase(p);
}

void Table::deletedescriber(const string & ss){
	map<const string, Group *>::iterator p;
	p = arr.find(ss);
	free(p->second);
	p->second = 0;
}

void Table::show()
{
	map<const string, Group *>::iterator p;
	for (p = arr.begin(); p != arr.end(); ++p)
	{
		cout << (*p).first << "\t";
		if (p.operator*().second != NULL) cout << *(p.operator*().second) << endl;
		else cout << "the describer doesn't excist :(" << endl;
	}
}

void Table::show_kaf(){
	int kaf;
	cout << "Enter the department: -->"; cin >> kaf;
	map<const string, Group *>::iterator p;
	for (p = arr.begin(); p != arr.end(); ++p)
	{
		if ( (*p).second->Get_kaf() == kaf){
			cout << (*p).first << "\t";
			if (p.operator*().second != NULL) cout << *(p.operator*().second) << endl;
			else cout << "the describer doesn't excist :(" << endl;
		}
	}
};

void Table::change_kont(const string &s){
	map<const string, Group *>::iterator p;
	p = arr.find(s);
	EveningBudget *D;
	EveningBudget E;
	Group *ptr = nullptr;
	if (p->second->Get_Form() == 2){
		D = dynamic_cast<EveningBudget*>((*p).second);
		D->Set_kont();
	}
	else{
		cout << "this group of another type. let's create new group with this name:)" << endl;
		arr.erase(p);
		ptr = &E;
		cin >> (*ptr);
		Table::insert(s, ptr);
	}
}

void Table::change_spec(const string &s){
	map<const string, Group *>::iterator p;
	p = arr.find(s);
	(*p).second->Set_spec();
}

void Table::change_years(const string &s){
	map<const string, Group *>::iterator p;
	p = arr.find(s);
	(*p).second->Set_years();
}

void Table::change_kaf(const string &s){
	map<const string, Group *>::iterator p;
	p = arr.find(s);
	(*p).second->Set_kaf();
}

void Table::change_n(const string &s){
	map<const string, Group *>::iterator p;
	p = arr.find(s);
	(*p).second->Set_n();
}

void Table::change_scholarship(const string &s){
	map<const string, Group *>::iterator p;
	p = arr.find(s);
	DayBudget *D;
	if (p->second->Get_Form() == 1){
		D = dynamic_cast<DayBudget*>((*p).second);
		D->Set_stip();
	}
	else cout << "You're wrong. It's not a DayBudget group\n" << endl;
}

void Table::change_scholars(const string &s){
	map<const string, Group *>::iterator p;
	p = arr.find(s);
	DayBudget *D;
	if ((*p).second->Get_Form() == 1){
		D = dynamic_cast<DayBudget*>((*p).second);
		D->Set_nstip();
	}
	else cout << "You're wrong. It's not a DayBudget group\n" << endl;
}

void Table::change_payment(const string &s){
	map<const string, Group *>::iterator p;
	p = arr.find(s);
	Pay *P;
	if ((*p).second->Get_Form() == 3){
		P = dynamic_cast<Pay*>((*p).second);
		P->Set_payment();
	}
	else cout << "You're wrong. It's not a Pay group\n" << endl;
}

int Table::get_report(){
	map<const string, Group *>::iterator p;
	DayBudget *D;
	Pay *P;
	int sum = 0;
	for (p = arr.begin(); p != arr.end(); ++p)
	{
		if ((*p).second->Get_Form() == 1){
			D = dynamic_cast<DayBudget*>((*p).second);
			sum = sum - (D->Get_nstip())*(D->Get_stip());
		}
		else if ((*p).second->Get_Form() == 3){
			P = dynamic_cast<Pay*>((*p).second);
			sum = sum + (P->Get_n())*(P->Get_payment());
		}
	}
	return sum;
}


Table& Table::operator = (const Table &a)
{
	map<const string, Group *>::iterator p;
	if (this != &a){
		for (p = arr.begin(); p != arr.end(); ++p)
			delete p->second;
		arr.clear();
		map<const string, Group *>::const_iterator pp;
		for (pp = a.arr.begin(); pp != a.arr.end(); ++pp)
			arr.insert(make_pair(p->first, p->second->clone()));
	}
	return *this;
}

void Table::insert(const string &s, const Group *f)
{
	map<const string, Group *>::iterator p = arr.find(s);
	if (p == arr.end()){
		pair<map<const string, Group *>::iterator, bool> pp =
			arr.insert(make_pair(s, f->clone()));
		if (!pp.second)
			throw exception("can't insert new item into map");
	}
	else {
		delete p->second;
		p->second = f->clone();
	}
}

const Group * Table::operator [ ](const string &s) const
{
	map<const string, Group *>::const_iterator p = arr.find(s);
	return (p == arr.end()) ? nullptr : p->second;
}

Table::Iterator Table::find(const string &s)
{
	map<const string, Group *>::iterator p = arr.find(s);
	return TableIt(p);
}

Table::Iterator Table::begin()
{
	return TableIt(arr.begin());
}

Table::Iterator Table::end()
{
	return TableIt(arr.end());
}



int Table::Iterator::operator !=(const TableIt &it) const
{
	return cur != it.cur;
}

int TableIt::operator ==(const TableIt &it) const
{
	return cur == it.cur;
}

pair<const string, Group *> & TableIt::operator *()
{
	return *cur;
}

TableIt & TableIt::operator ++()
{
	++cur;
	return *this;
}

TableIt TableIt::operator ++(int)
{
	TableIt res(*this);
	++cur;
	return res;
}